﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp26
{
    class CareTaker
    {

        public Memento PreviousState { get; set; }

        List<Memento> list = new List<Memento>();

        public void AddMemento(Memento memento)
        {
            if(memento != null)
            {
                list.Add(memento);
            }
        }

        public void RemoveMemento(Memento memento)
        {
            list.Remove(memento);
        }
    }
}
