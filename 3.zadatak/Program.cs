﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp26
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime time = new DateTime(2020, 11, 20);
            ToDoItem item = new ToDoItem("Shop", "buy milk", time);

            CareTaker taker = new CareTaker();

            Console.WriteLine();
            Console.WriteLine(item);
            item.ChangeTask("buy eggs");
            item.ChangeTimeDue(new DateTime(2020, 5, 14));

            Console.WriteLine();

            Console.WriteLine("Store state: ");
            taker.AddMemento(item.StoreState());
            Console.WriteLine(item);            
        }
    }
}
