﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp26
{
    class Program
    {
        static void Main(string[] args)
        {
            Note paper = new Note("Don't forget!", "Buy eggs and milk.");
            Note paper_1 = new Note("Exam!", "RPPOON - exam");

            Notebook notebook = new Notebook();
            notebook.AddNote(paper);
            notebook.AddNote(paper_1);

            Console.WriteLine("Print without loop: ");

            Console.WriteLine("First note: ");
            Console.WriteLine(paper.Title);
            Console.WriteLine(paper.Text);

            Console.WriteLine();

            Console.WriteLine("Second note: ");
            Console.WriteLine(paper_1.Title);
            Console.WriteLine(paper_1.Text);

            IAbstractIterator iterator = notebook.GetIterator();

            Console.WriteLine();

            Console.WriteLine("Print using loop: ");

            Note i = iterator.First();
            while(iterator.IsDone == false)
            {
                i.Show();
                i = iterator.Next();
            }

        }
    }
}
