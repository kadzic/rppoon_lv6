﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp26
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product = new Product("Headphones", 100.00);
            Product product_1 = new Product("Keyboard", 50.00);

            Box products = new Box();
            products.AddProduct(product);
            products.AddProduct(product_1);    

            Console.WriteLine("Print without loop: ");

            Console.WriteLine("First product: ");
            Console.WriteLine(product.Description.ToString()); ;
            Console.WriteLine(product.Price.ToString());

            Console.WriteLine();

            Console.WriteLine("Second product: ");
            Console.WriteLine(product_1.Description.ToString());
            Console.WriteLine(product_1.Price.ToString());

            IAbstractIterator_ iterator_ = products.GetIterator();

            Console.WriteLine();

            Console.WriteLine("Print using loop: ");

            Product i = iterator_.First();
            while(iterator_.IsDone == false)
            {
                Console.WriteLine(i.ToString());
                i = iterator_.Next();
            }
        }
    }
}
