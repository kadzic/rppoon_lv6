﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp26
{
    interface IAbstractIterator_
    {
        Product First();
        Product Next();
        bool IsDone { get; }
        Product Current { get; }
    }
}
